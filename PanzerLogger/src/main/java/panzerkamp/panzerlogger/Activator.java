package panzerkamp.panzerlogger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import panzerkamp.logger.Logger;

public class Activator implements BundleActivator {

    PanzerLogger pLogger = null;
    
    
    public void start(BundleContext context) throws Exception {
        pLogger = new PanzerLogger();
        context.registerService(Logger.class.getName(), pLogger, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
