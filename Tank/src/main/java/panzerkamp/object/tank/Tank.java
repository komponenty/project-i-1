package panzerkamp.object.tank;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.util.*;
import maybach.collision.TestCollision;
import maybach.engine.Engine;
import maybach.object.*;
import panzerkamp.main.Constans;
import panzerkamp.object.*;
import panzerkamp.object.bullet.*;

public abstract class Tank extends Shape implements ITank, KeyListener, MouseMotionListener, MouseListener, EreignisObjekts {

    // Tank Properties
    // Tank Eigenschaft
    ////////////////////////////////////////////////////
    // reload speed
    protected double ladenGeschwindikeit;
    // fire speed
    protected double feuerGeschwindigkeit;
    // rotate speed
    protected double drehungGeschwindigkeit;
    // move speed
    protected double bewegungsGeschwindigkeit;
    // health
    protected double gesundheit = 0;
    protected double maxgesundheit = 0;
    // fire strength
    protected double feuerKraft;
    // shot distance
    protected double schussDistanz;
    // cost
    protected double preis;

    // virtdistance
    protected double distx = -70 / 2;
    protected double disty = -70 / 2;

    // Last states
    // letzte Stand
    ///////////////////////////////////////////////////
    // last angle
    protected double letzteWinkel;
    // last position
    protected double letzteX = 0;
    protected double letzteY = 0;

    protected int mausletzteX = 0;
    protected int mausletzteY = 0;
    protected double mausleztzewinkel = 0;
    // shot
    protected boolean feuer;
    // moving
    protected boolean rechtBewegen;
    protected boolean linksBewegen;
    protected boolean hochBewegen;
    protected boolean herabBewegen;
    protected boolean space;

    // Timers
    ///////////////////////////////////////////////////
    // shot timer
    protected long feuerTimer;
    protected long timer;
    protected long bewegungTimer;

    // Sprites
    ///////////////////////////////////////////////////
    // move sprite
    protected Image[] bewegtbild;

    // Kugel Type
    ///////////////////////////////////////////////////
    protected IKugel kugelArt;

    // Methods
    ///////////////////////////////////////////////////
    // Kugels
    ///////////////////////////////////////////////////
    protected IKugel geschoss;

    public Tank(int x, int y, double winkel, int seite) {
        super(x, y, winkel, seite);
        this.name = "Tank";

        this.letzteWinkel = winkel;
        this.letzteX = 0;
        this.letzteY = 0;
        this.feuer = false;
        this.feuerTimer = 0;
        this.bewegungTimer = 0;
        this.selected = false;

        this.herabBewegen = false;
        this.hochBewegen = false;
        this.rechtBewegen = false;
        this.linksBewegen = false;
        this.space = false;

        this.setCollisionPoints(new ArrayList<Point>());
        this.addCollisionPoints(new Point(x, y));
        this.addCollisionPoints(new Point(x, y));
        this.addCollisionPoints(new Point(x, y));
        this.addCollisionPoints(new Point(x, y));

    }

    
    @Override
    public void Bewegen(long dt) {

        double distance = this.bewegungsGeschwindigkeit / dt;
        double winkeltg = Math.tan(this.angle);

        //backup 
        int tempx = this.x;
        int tempy = this.y;
        double templetztex = this.letzteX;
        double templetztey = this.letzteY;
        double tempwinkel = this.angle;

        // adding angle to tank tube
        double tg = Math.atan2(-this.mausletzteX + this.x, this.mausletzteY - this.y) + Math.PI / 2;
        this.mausleztzewinkel = tg;
        // end adding 

        // {distance^2 = x^2 + y^2
        // {y = x*tg(angle)
        // => distance = x * sqrt ( 1 + tg(angle)^2 )		
        double x = (distance / Math.sqrt(1 + Math.pow(winkeltg, 2)));
        double y = (distance * winkeltg / Math.sqrt(Math.pow(winkeltg, 2) + 1));

        // update collission points
        updateKollision();
        //System.out.printf("Befor Update x: %d\t y: %d \t angle: %2.2f\tlx: %2.2f\tly: %2.2f\tdist: %2.2f\tdt: %d\tx: %2.2f\ty: %2.2f \t winkeltg: %2.2f \n", this.x, this.y, this.angle, this.letzteX, this.letzteY, distance, dt, x, y, winkeltg);

        if (Math.abs(this.angle) > Math.PI / 2) {
            x = -x;
            y = -y;
        }

        this.letzteX += x;
        this.letzteY += y;
        if (Math.abs(this.letzteX) > 1 || Math.abs(this.letzteY) > 1) {
            if (this.herabBewegen) {

                this.x -= (int) this.letzteX;
                this.y -= (int) this.letzteY;
            } else if (this.hochBewegen) {
                this.x += (int) this.letzteX;
                this.y += (int) this.letzteY;
            }
        }

        if (this.linksBewegen) {
            this.Drehen(-drehungGeschwindigkeit / dt);
        }
        if (this.rechtBewegen) {
            this.Drehen(+drehungGeschwindigkeit / dt);
        }

        // updating collision points
        updateKollision();

        // checking collision with shapes
        Map<String, String> col = this.checkCollision(Constans.MSG_MOVE);
        if (col == null) {
        } else if (col.containsKey(Constans.MSG_OBJECT_ISBLOCKABLE)) {
            //System.out.printf("After Update x: %d\t y: %d \t angle: %2.2f\tlx: %2.2f\tly: %2.2f\tdist: %2.2f\tdt: %d\tx: %2.2f\ty: %2.2f \t winkeltg: %2.2f \n", this.x, this.y, this.angle, this.letzteX, this.letzteY, distance, dt, x, y, winkeltg);

            if (this.herabBewegen || this.hochBewegen) {
                this.x = tempx;
                this.y = tempy;
                this.letzteX = templetztex;
                this.letzteY = templetztey;
            }
            if (this.rechtBewegen || this.linksBewegen) {
                this.angle = tempwinkel;
            }
        }
        updateKollision();
        // end checking

        this.letzteX = Math.abs(this.letzteX) > 1 ? this.letzteX - (int) this.letzteX : this.letzteX;
        this.letzteY = Math.abs(this.letzteY) > 1 ? this.letzteY - (int) this.letzteY : this.letzteY;
    }

    @Override
    public void Drehen(double winkel) {
        this.letzteWinkel = this.angle;
        this.angle += winkel;
        this.angle = Math.abs(this.angle) > Math.PI ? -this.angle + winkel : this.angle;

    }

    private void updateKollision() {
        TestCollision.updateSquareKollision(this.getCollisionPoints(), new Point(this.x, this.y), this.angle, this.getBewegtbild()[0], true);
    }

    protected void updateDist(Image img) {
        this.distx = -img.getWidth(null) / 2;
        this.disty = -img.getHeight(null) / 2;
    }

    @Override
    public void Feuer() {
        if (this.feuerTimer >= this.feuerGeschwindigkeit) {
            this.feuerTimer = 0;
            Engine e = Engine.getEngine();
            // get bullet layer
            Shape s = this.geschoss.clone(this.x, this.y, this.mausleztzewinkel, this.getSide());
            e.RegisterShape(s, Constans.LAYER_BULLETS);
        }
    }

    @Override
    public Map<String, String> getMsg(String type, Shape s, Map<String, String> dict) {
        HashMap<String, String> res = new HashMap<String, String>();

        if (type == Constans.MSG_HIT && dict.containsKey("seite") && Integer.parseInt(dict.get("seite")) != this.getSide()) {
            if (this.istBlockierBar() || this.istZerstorBar()) {
                res.put(Constans.MSG_DESTROY, "this");
            }
            if (this.istZerstorBar()) {
                if (dict.containsKey("schaden")) {
                    double dmg = java.lang.Double.parseDouble(dict.get("schaden"));

                    this.maxgesundheit -= dmg;

                    if (this.maxgesundheit <= 0) {
                        this.destroy();
                    } else {
                        //Sound.play("hit.wav");
                    }
                }
            }

            return res;
        }

        if (type == Constans.MSG_ROTATE || type == Constans.MSG_MOVE) {
            if (this.istBlockierBar()) {
                res.put(Constans.MSG_OBJECT_ISBLOCKABLE, "this");
            }
            return res;
        }

        return res;
    }

    @Override
    public Map<String, String> getInfo() {
        HashMap<String, String> info = new HashMap<String, String>();

        info.put("winkel", String.valueOf(this.angle));
        info.put("letzteX", String.valueOf(this.x));
        info.put("letzteY", String.valueOf(this.y));
        info.put("name", this.getName());
        info.put("seite", String.valueOf(this.getSide()));
        return info;
    }

    @Override
    public void destroy() {
        super.destroy();
        //Sound.play("bomb_exploding.wav");
    }

    // is blockAble()
    @Override
    public boolean istBlockierBar() {
        return true;
    }

    // is destroyAble()
    @Override
    public boolean istZerstorBar() {
        return true;
    }

    @Override
    public void update(long dt) {
        if (dt <= 0) {
            return;
        }
        this.Bewegen(dt);
        this.bewegungTimer += dt;

        if (this.feuerTimer < this.feuerGeschwindigkeit) {
            this.feuerTimer += dt;
        }
        this.timer += dt;

    }

    @Override
    public void paint(Graphics g) {
        // img
        Image img = this.getBewegtbild()[0];

        // painting tank in center of this.x and this.y
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.GREEN);
        g2d.fillRect(this.x - (int) (img.getWidth(null) / 2), this.y + img.getHeight(null), (int) (img.getWidth(null) * (this.maxgesundheit / this.gesundheit)), 3);

        AffineTransform xform = new AffineTransform();
        xform.translate(this.x, this.y);
        xform.rotate(this.angle);
        xform.translate(this.distx, this.disty);
        // drawing tank
        g2d.drawImage(img, xform, null);
        // drawing tube
        AffineTransform xform2 = new AffineTransform();
        xform2.translate(this.x, this.y);
        xform2.rotate(this.mausleztzewinkel);
        xform2.translate(this.distx, this.disty);

        g2d.drawImage(this.getBewegtbild()[1], xform2, null);

//		for ( int i = 0; i < 4; i++)
//		{
//			Point p = this.getCollisionPoints().get(i);
//			Point p1 = this.getCollisionPoints().get((i+1)%4);
//			g2d.drawLine(p.x, p.y, p1.x, p1.y);
//		}
        // Health Bar
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        switch (keyCode) {
            case KeyEvent.VK_UP:
            case KeyEvent.VK_W:
                this.hochBewegen = true;
                break;
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_S:
                this.herabBewegen = true;
                break;
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_A:
                this.linksBewegen = true;
                break;
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_D:
                this.rechtBewegen = true;
                break;
            case KeyEvent.VK_SPACE:
                this.space = true;

                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();
        switch (keyCode) {
            case KeyEvent.VK_UP:
            case KeyEvent.VK_W:
                this.hochBewegen = false;
                break;
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_S:
                this.herabBewegen = false;
                break;
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_A:
                this.linksBewegen = false;
                break;
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_D:
                this.rechtBewegen = false;
                break;
            case KeyEvent.VK_SPACE:
                this.space = false;
                this.Feuer();
                break;
        }
    }

    @Override
    public void keyTyped(KeyEvent arg0) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub
        this.Feuer();
        System.out.println("Clicked");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub
        this.mausletzteX = e.getX();
        this.mausletzteY = e.getY();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub
        System.out.println("Mouse press");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub
        System.out.println("Mouse repress");
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        // TODO Auto-generated method stub
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        // TODO Auto-generated method stub
        this.mausletzteX = e.getX();
        this.mausletzteY = e.getY();
    }

    /**
     * @return the bewegtbild
     */
    @Override
    public Image[] getBewegtbild() {
        return bewegtbild;
    }

    /**
     * @param bewegtbild the bewegtbild to set
     */
    @Override
    public void setBewegtbild(Image[] bewegtbild) {
        this.bewegtbild = bewegtbild;
    }

    /**
     * @return the logo
     */
    @Override
    public Image getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    @Override
    public void setLogo(Image logo) {
        this.logo = logo;
    }
}
