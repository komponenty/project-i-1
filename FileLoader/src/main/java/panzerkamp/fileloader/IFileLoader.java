/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package panzerkamp.fileloader;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

/**
 *
 * @author thegrymek
 */
public interface IFileLoader {

    File getFileFromDialog();
    URL getProjectResource(Class<?> clazz, String resource);
    InputStream getProjectResourceAsStream(Class<?> clazz, String resource);
    
}
