package panzerkamp.fileloader;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

public class Activator implements BundleActivator {

    public void start(BundleContext context) throws Exception {
        // TODO add activation code here
        context.registerService(IFileLoader.class.getName(), new FileLoader(), null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
        ServiceReference fileloader = context.getServiceReference(IFileLoader.class.getName());
        context.ungetService(fileloader);
    }

}
