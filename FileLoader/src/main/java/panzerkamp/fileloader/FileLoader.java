/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panzerkamp.fileloader;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import javax.swing.JFileChooser;

/**
 *
 * @author thegrymek
 */
public class FileLoader implements IFileLoader {

    public FileLoader() {

    }

    @Override
    public URL getProjectResource(Class<?> clazz, String resource) {
        return clazz.getClassLoader().getResource(resource);
    }

    @Override
    public InputStream getProjectResourceAsStream(Class<?> clazz, String resource) {
        return clazz.getClassLoader().getResourceAsStream(resource);
    }

    @Override
    public File getFileFromDialog() {
        final JFileChooser dialog = new JFileChooser();

        int dialogStatus = dialog.showOpenDialog(null);
        if (dialogStatus == JFileChooser.APPROVE_OPTION) {
            return dialog.getSelectedFile();
        } else {
            return null;
        }
    }

}
