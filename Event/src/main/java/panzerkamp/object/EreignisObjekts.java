package panzerkamp.object;

public interface EreignisObjekts {
	// is blockAble()
	boolean istBlockierBar();
	// is destroyAble()
	boolean istZerstorBar();
}
