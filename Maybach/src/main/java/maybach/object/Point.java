package maybach.object;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;

public class Point extends Shape {

	
	public Point(int x, int y)
	{
		super(x,y);
		this.name = "Point";
	}
	
	@Override
	public void paint(Graphics g)
	{
        Graphics2D g2d = (Graphics2D) g;
        Color c = g2d.getColor();
        g2d.setColor(this.getColor());
        g2d.drawLine(this.x, this.y, this.x, this.y);
        g2d.setColor(c);
	}
}
