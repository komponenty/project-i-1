package maybach.object;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;

public class Rectangle extends Shape {

	Color color;
	
	protected int width;
	protected int height;
	
	public Rectangle(int x, int y, int width, int height)
	{
		super(x,y);
		this.name = "Rectangle";
		this.width = width;
		this.height = height;
	}
	
	
	@Override
	public void paint(Graphics g)
	{
        Graphics2D g2d = (Graphics2D) g;
        Color c = g2d.getColor();
        g2d.setColor(this.getColor());
        g2d.drawRect( this.x, this.y, this.width, this.height);
        g2d.setColor(c);
	}
}
