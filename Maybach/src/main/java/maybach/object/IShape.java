/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maybach.object;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.util.List;
import java.util.Map;

/**
 *
 * @author thegrymek
 */
public interface IShape {

    void addCollisionPoints(Point p);

    void addCollisionPoints(int x, int y);

    void destroy();

    boolean equals(Object o);

    // Collisions
    /////////////////////////////////////////////////////////////////////
    List<Point> getCollisionPoints();

    Color getColor();

    ////////////////////////////////////////////////////////
    Color getFillColor();

    Map<String, String> getInfo();

    Image getLogo();

    Map<String, String> getMsg(String type, Shape s, Map<String, String> dict);

    String getName();

    int getSide();

    boolean getVisible();

    boolean isVisible();

    void paint(Graphics g);

    void removeCollisionPoints(int l);

    void removeCollisionPoints(Point p);

    void removeCollisionPoints(int x, int y);

    void setBackColor(Color c);

    void setCollisionPoints(List<Point> newCollisionPoints);

    void setFillColor(Color c);

    void setLogo(Image img);

    void setSelected();

    void setSide(int s);

    void setVisible(boolean v);

    void setX(int x);

    void setY(int y);

    void unSelected();

    void update(long dt);
    
}
