package maybach.layer;

import java.awt.Graphics;
import java.util.*;
import maybach.object.Shape;

public class Layer implements ILayer {

    private LinkedList<Shape> shapes;
    private boolean isVisible = true;
    private String name = "Layer";

    public Layer(String n) {
        name = n;
        shapes = new LinkedList<Shape>();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void update(long dt) {
        for (int i = 0; i < this.shapes.size(); i++) {
            Shape s = this.shapes.get(i);

            if (s != null && (s.isVisible())) {
                s.update(dt);
            }
        }
    }

    public void paint(Graphics g) {
        for (int i = 0; i < this.shapes.size(); i++) {
            Shape s = this.shapes.get(i);

            if (s != null && (s.isVisible())) {
                s.paint(g);
            }
        }
    }

    public boolean getVisible() {
        return this.isVisible;
    }

    public boolean isVisible() {
        return this.isVisible;
    }

    public void setVisible(boolean visible) {
        this.isVisible = visible;
    }

    public void translate(int dx, int dy) {
        for (Shape s : shapes) {
            s.translate(dx, dy);
        }

    }

    public Shape[] getShapes() {
        return shapes.toArray(new Shape[shapes.size()]);
    }

    public Shape getShape(Shape s) {
        for (Shape shape : shapes) {
            if (s.equals(shape)) {
                return shape;
            }
        }
        return null;
    }

    @Override
    public Shape getShape(int s) {
        if (this.shapes.size() > s) {
            return null;
        } else {
            return this.shapes.get(s);
        }
    }

    @Override
    public void RegisterShape(Shape s) {
        shapes.add(s);
    }

    @Override
    public void UnregisterShape(Shape s) {
        for (int i = 0; i < this.shapes.size(); i++) {
            if (shapes.get(i) == s) {
                shapes.set(i, null);
            }
        }
    }

    @Override
    public void UnregisterShape(String name) {
        for (Shape s : shapes) {
            if (s.getName() == name) {
                s = null;
            }
        }
    }

    public void clean() {
        Iterator<Shape> iter = this.shapes.iterator();
        while (iter.hasNext()) {
            Shape s = iter.next();
            if (s == null) {
                iter.remove();
            }
        }
    }

    public void clear() {
        shapes.clear();
    }

}
