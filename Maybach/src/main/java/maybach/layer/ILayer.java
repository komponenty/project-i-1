package maybach.layer;


import maybach.object.Shape;

public interface ILayer {
	void RegisterShape(Shape s);
	void UnregisterShape(Shape s);
	void UnregisterShape(String name);
	void update(long dt);
	
	Shape getShape(Shape s);
	Shape getShape(int s);
	Shape [] getShapes();
	String getName();
	void setName(String name);
	boolean getVisible();
	boolean isVisible();
	void setVisible(boolean visible);
	void clean();
        void clear();
}
