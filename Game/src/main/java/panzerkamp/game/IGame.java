/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panzerkamp.game;

import maybach.engine.Engine;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Deactivate;
import panzerkamp.object.tank.Tank;

/**
 *
 * @author thegrymek
 */
public interface IGame {

    void RegisterPlayer(Tank panzer);

    @Activate
    void Start();

    @Deactivate
    void Stop();

    void UnregisterPlayer(Tank panzer);

    void bindEngine(Engine e);

    void unbindEngine();
    
}
