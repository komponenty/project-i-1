package panzerkamp.object.tankt3485;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import panzerkamp.main.Constans;
import panzerkamp.object.tank.Tank;

public class Activator implements BundleActivator {

    private t3485 t3485;
    private BundleContext ctx;

    public void start(BundleContext context) throws Exception {
        this.t3485 = new t3485(0,0,0, Constans.SIDE_PLAYER);
        this.ctx = context;
        ctx.registerService(Tank.class.getName(), this.t3485, null);
    }
    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
