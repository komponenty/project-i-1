package panzerkamp.object.tankt3485;

import java.awt.Image;

import javax.swing.ImageIcon;

import panzerkamp.main.Constans;
import panzerkamp.object.bulleteinfache.EinFacheKugel;
import panzerkamp.object.tank.Tank;

public class t3485 extends Tank {

    public t3485(int x, int y, double winkel, int seite) {
        super(x, y, winkel, seite);
        
        this.name="t3485";
        // reload speed
        this.ladenGeschwindikeit = 70;
        // fire speed
        this.feuerGeschwindigkeit = 700;
        // rotate speed
        this.drehungGeschwindigkeit = 0.3;
        // move speed
        this.bewegungsGeschwindigkeit = 22;
        // health
        this.gesundheit = 3000;
        this.maxgesundheit = 3000;
        // fire strength
        this.feuerKraft = 400;
        // shot distance
        this.schussDistanz = 500;
        // cost
        this.preis = 100;

        // kugelType
        this.geschoss = new EinFacheKugel(this.x, this.y, 0, this.getSide(), this.schussDistanz, this.feuerKraft);
        //Sprites
        this.bewegtbild = new Image[]{
            new ImageIcon(this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_TANKS + "t-34-85-korper.png")).getImage(),
            new ImageIcon(this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_TANKS + "t-34-85-fass.png")).getImage()
        };

        this.logo = new ImageIcon(this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_TANKS + "t-34-85.png")).getImage();

        this.updateDist(this.bewegtbild[0]);
    }

}
