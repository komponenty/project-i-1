package panzerkamp.object.tankPanther;

import java.awt.Image;
import javax.swing.ImageIcon;
import panzerkamp.main.Constans;
import panzerkamp.object.bulleteinfache.EinFacheKugel;
import panzerkamp.object.tank.Tank;

public class Panther extends Tank {

    public Panther(){
        this(0,0,0, Constans.SIDE_PLAYER);
    }
    public Panther(int x, int y, double winkel, int seite) {
        super(x, y, winkel, seite);
        
        this.name ="Panther";
        // reload speed
        this.ladenGeschwindikeit = 70;
        // fire speed
        this.feuerGeschwindigkeit = 800;
        // rotate speed
        this.drehungGeschwindigkeit = 0.3;
        // move speed
        this.bewegungsGeschwindigkeit = 22;
        // health
        this.gesundheit = 4000;
        this.maxgesundheit = 4000;
        // fire strength
        this.feuerKraft = 400;
        // shot distance
        this.schussDistanz = 800;
        // cost
        this.preis = 100;

        // kugelType
        this.geschoss = new EinFacheKugel(this.x, this.y, 0, this.getSide(), this.schussDistanz, this.feuerKraft);
        //Sprites
        this.bewegtbild = new Image[]{
            new ImageIcon(this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_TANKS + "panther-korper.png")).getImage(),
            new ImageIcon(this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_TANKS + "panther-fass.png")).getImage()
        };

        this.logo = new ImageIcon(this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_TANKS + "panther.png")).getImage();

        
        this.updateDist(this.bewegtbild[0]);
    }

}
