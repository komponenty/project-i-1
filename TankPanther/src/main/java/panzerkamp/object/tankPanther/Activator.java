package panzerkamp.object.tankPanther;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import panzerkamp.object.tank.Tank;

public class Activator implements BundleActivator {

    private Panther tPanther;
    private BundleContext ctx;

    public void start(BundleContext context) throws Exception {
        this.tPanther = new Panther();
        this.ctx = context;
        ctx.registerService(Tank.class.getName(), this.tPanther, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }
    
}
