package panzerkamp.logger;

public interface Logger {

	void log(Object src, String msg);

	void addLogListener(LogListener listener);

	void removeLogListener(LogListener listener);
}
