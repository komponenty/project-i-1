package panzerkamp.logger;

public interface LogListener {
	void performLogEvent(LogEvent evt);
}
