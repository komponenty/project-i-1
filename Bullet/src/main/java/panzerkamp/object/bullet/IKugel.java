package panzerkamp.object.bullet;

import maybach.object.Shape;

public interface IKugel {

	Shape clone(int x, int y, double winkel, int seite);


}
