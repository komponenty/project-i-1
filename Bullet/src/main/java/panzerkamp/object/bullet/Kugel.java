package panzerkamp.object.bullet;

import maybach.collision.TestCollision;
import maybach.engine.Engine;
import maybach.object.Shape;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import panzerkamp.main.Constans;


public abstract class Kugel extends Shape implements IKugel {

	protected int bewegungsGeschwindigkeit;
	protected double letzteX;
	protected double letzteY;
	protected double winkeltg;
	protected double distanz;
	protected double kraft;
	// Sprites
	///////////////////////////////////////////////////
	// move sprite
	protected Image [] bewegtbild;
		
	protected double distx;
	protected double disty;
	
	public Kugel(int x, int y, double winkel, int seite, int bewegungsGeschwindigkeit, double distanz, double kraft)
	{
		super(x,y,winkel,seite);
		this.name = "Kugel";
		this.bewegungsGeschwindigkeit = bewegungsGeschwindigkeit;
		this.winkeltg = Math.tan(this.angle);
		this.distanz = distanz;
		this.kraft = kraft;
		this.setCollisionPoints(new ArrayList<Point>());
		this.addCollisionPoints(new Point(x,y));
		this.addCollisionPoints(new Point(x,y));
		this.addCollisionPoints(new Point(x,y));
		this.addCollisionPoints(new Point(x,y));
		
		
	}
	
	protected void updateDist(Image img)
	{
		this.distx = -img.getWidth(null)/2;
		this.disty = -img.getHeight(null)/2;
	}
	
	public void paint(Graphics g)
	{
		// painting tank in center of this.x and this.y
		Graphics2D g2d = (Graphics2D) g;
		AffineTransform xform = new AffineTransform();
		xform.translate(this.x, this.y);
		xform.rotate(this.angle);
		xform.translate(this.distx, this.disty);
		g2d.drawImage(this.bewegtbild[0], xform, null);
//		g2d.setBackground(Color.black);
//		for ( int i = 0; i < 4; i++)
//		{
//			Point p = this.collisionPoints.get(i);
//			Point p1 = this.collisionPoints.get((i+1)%4);
//			g2d.drawLine(p.x, p.y, p1.x, p1.y);
//		}
	}
	
	public void updateKollision()
	{
		TestCollision.updateSquareKollision(this.getCollisionPoints(), new Point(this.x, this.y), this.angle, this.bewegtbild[0], true);
	}
	
	public void update(long dt)
	{
		super.update(dt);
		if ( dt <= 0 ) return;
			
		if ( this.distanz < 0 )
		{
			Engine.getEngine().UnregisterShape(this);
			return;
		}
		
		updateKollision();

		double distance = this.bewegungsGeschwindigkeit / dt;
		// {distance^2 = x^2 + y^2
		// {y = x*tg(angle)
		// => distance = x * sqrt ( 1 + tg(angle)^2 )		
		double x =  ( distance / Math.sqrt(1 + Math.pow(winkeltg, 2)));
		double y =  ( distance * winkeltg / Math.sqrt( Math.pow(winkeltg, 2) + 1 ));
		if ( Math.abs(this.angle) > Math.PI / 2)
		{
			x = -x;
			y = -y;
		}
		
		this.letzteX += x;
		this.letzteY += y;
		if ( Math.abs(this.letzteX) > 1 || Math.abs(this.letzteY) > 1 )
		{
			int tempx = (int) this.letzteX;
			int tempy = (int) this.letzteY;
			this.x += tempx;
			this.y += tempy;
			
			Map<String,String> res = this.checkCollision(Constans.MSG_HIT);
			if ( res == null )
			{
				
			}
			else if ( res.containsKey(Constans.MSG_DESTROY) || res.containsKey(Constans.MSG_OBJECT_ISBLOCKABLE))
			{
				this.destroy();
			}
			
			this.distanz -= (int)(Math.sqrt(Math.pow(tempx, 2)+Math.pow(tempy, 2)));
		}
		this.letzteX = Math.abs(this.letzteX) > 1 ? this.letzteX - (int)this.letzteX : this.letzteX;
		this.letzteY = Math.abs(this.letzteY) > 1 ? this.letzteY - (int)this.letzteY : this.letzteY;
		
	}
	
	public Map<String,String> getInfo()
	{
		HashMap<String,String> info = new HashMap<String,String>();
		
		info.put("schaden", String.valueOf(this.kraft));
		info.put("winkel", String.valueOf(this.winkeltg));
		info.put("letzteX", String.valueOf(this.x));
		info.put("letzteY", String.valueOf(this.y));
		info.put("name", this.getName());
		info.put("seite", String.valueOf(this.getSide()));
		return info;
		
	}
	
	
	@Override
	public Shape clone(int x, int y, double winkel, int seite)
	{
		
		return this;
	}
	
	
	
}
