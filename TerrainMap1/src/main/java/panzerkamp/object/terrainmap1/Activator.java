package panzerkamp.object.terrainmap1;

import java.awt.Image;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import panzerkamp.object.terrain.Terrain;

public class Activator extends Terrain implements BundleActivator {

    private BundleContext ctx;

    public Activator() {
        super("Auschwitz", "logo.png", "map1.xml");
        this.sx = 500;
        this.sy = 500;
    }
    
    public void start(BundleContext context) throws Exception {
        this.LoadContext(context);
        context.registerService(Terrain.class.getName(), this, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

    
}
